# EXAPUNKS Solutions
A repository of how I've gone about solving puzzles in Exapunks, more for me to quickly reference.

No cheating, just referring to the manual as intended.

If you haven't played the game before, please don't spoil yourself by reading the solutions. I want others to compare and for myself to have a way to improve.
